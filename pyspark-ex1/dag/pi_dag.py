from datetime import datetime, timedelta
import pendulum
from airflow import DAG
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator
from airflow.models import Variable

local_tz = pendulum.timezone("Asia/Ho_Chi_Minh")

default_args = {
    'owner': 'trungphan',
    'depends_on_past': False,
    'start_date': datetime.combine(datetime.today() - timedelta(7),
                                  datetime.min.time()),
    'email': ['pqtr133@gmail.com'],
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 0,
    'retry_delay': timedelta(minutes=5)
}
dag = DAG(dag_id='pi_dag',
          default_args=default_args,
          catchup=False,
          schedule_interval="*/5 * * * *")

pyspark_app_home = Variable.get("PYSPARK_APP_HOME")

pi_process = SparkSubmitOperator(task_id='pi_process',
                                              conn_id='spark_default',
                                              application=f'{pyspark_app_home}/pyspark-ex1/pi.py',
                                              total_executor_cores=2,
                                            #   packages="io.delta:delta-core_2.12:0.7.0,org.apache.spark:spark-sql-kafka-0-10_2.12:3.0.0",
                                              executor_cores=1,
                                              executor_memory='1g',
                                              driver_memory='1g',
                                              name='pi_process',
                                              execution_timeout=timedelta(minutes=10),
                                              dag=dag
                                              )

status_api_demo = SparkSubmitOperator(task_id='status_api_demo',
                                              conn_id='spark_default',
                                              application=f'{pyspark_app_home}/pyspark-ex1/status_api_demo.py',
                                              total_executor_cores=2,
                                            #   packages="io.delta:delta-core_2.12:0.7.0,org.apache.spark:spark-sql-kafka-0-10_2.12:3.0.0",
                                              executor_cores=1,
                                              executor_memory='1g',
                                              driver_memory='1g',
                                              name='status_api_demo',
                                              execution_timeout=timedelta(minutes=10),
                                              dag=dag
                                              )
pi_process >> [status_api_demo]
